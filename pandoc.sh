echo "Benjamin BIGEY - [bigey@etud.insa-toulouse.fr](mailto:bigey@etud.insa-toulouse.fr)" > report.md
echo "" >> report.md
cat TP_1/report.md >> report.md
echo "" >> report.md
#echo "<div style=\"page-break-after: always;\"></div>" >> report.md
echo "\pagebreak" >> report.md
echo "" >> report.md
cat TP_2/report.md >> report.md
pandoc -s -S -V geometry:margin=1in -f markdown report.md -o report.pdf && firefox report.pdf
rm report.md
