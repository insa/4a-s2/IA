# TP1
## Familiarisation avec le problème du Taquin 3x3

**1.2 a) Quelle clause Prolog permettrait de représenter la situation finale du Taquin 4x4 ?**
```prolog
final_state4([[a, b, c, d],
              [e, f, g, h],
              [i, j, k, l],
              [m, n, o, vide]]).
```

**1.2 b) À quelles questions permettent de répondre les requêtes suivantes :**
* `initial_state(Ini), nth1(L,Ini,Ligne), nth1(C,Ligne, d).` : Retourne la position de `d` dans l'état initial (L = 2, C = 3)
* `final_state(Fin), nth1(3,Fin,Ligne), nth1(2,Ligne,P).` : Retourne le caractère à la position (L = 3, C = 2) dans l'état final (`f`)

**1.2 c) Quelle requête Prolog permettrait de savoir si une pièce donnée P *(ex : a)* est bien placée dans *U0* (par rapport à F) ?**
```prolog
initial_state(Ini), nth1(L,Ini,Ligne), nth1(C,Ligne, a), final_state(Fin), nth1(L,Fin,Ligne), nth1(C,Ligne,P).
```
J'ai déclaré la fonction `mal_placee(P, U)` qui retourne `faux` si `P` est bien placée dans `U`.

**1.2 d) Quelle requête permet de trouver une situation suivante de l'état initial du Taquin 3x3 ?**
```prolog
initial_state(Ini), rule(Rule, Cost, Ini, NextState).
Ini = [[b, h, c], [a, f, d], [g, vide, e]],
Rule = up,
Cost = 1,
NextState = [[b, h, c], [a, vide, d], [g, f, e]] ;
Ini = [[b, h, c], [a, f, d], [g, vide, e]],
Rule = left,
Cost = 1,
NextState = [[b, h, c], [a, f, d], [vide, g, e]] ;
Ini = [[b, h, c], [a, f, d], [g, vide, e]],
Rule = right,
Cost = 1,
NextState = [[b, h, c], [a, f, d], [g, e, vide]] ;
```

**1.2 e) Quelle requête permet d'avoir ces 3 réponses regroupées dans une liste ?**
```prolog
initial_state(Ini), findall(Rule, rule(Rule, 1, Ini, NextState), Result).
Result = [up, left, right].
```

**1.2 f) Quelle [A, S] tels que S est la situation qui résulte de laction A en U0 ?**
```prolog
initial_state(Ini), findall([Rule, NextState], rule(Rule, 1, Ini, NextState), Result).
Ini = [[b, h, c], [a, f, d], [g, vide, e]],
Result = [[up, [[b, h, c],
                [a, vide, d],
                [g, f, e]]],
          [left, [[b, h, c],
                  [a, f, d],
                  [vide, g|...]]],
          [right, [[b, h, c],
                   [a, f|...],
                   [g|...]]]].
```
La fonction `next_states(ResultList, State)` permets de réaliser cette requête.

## Développement des 2 heuristiques
### Heuristique 1
Son but est de compter le nombre de tuiles mal placées. Son implémentation se fait en deux temps : avoir une fonction pour déterminer si une tuile est mal placée, puis compter le nombre de tuiles qui le sont.

```prolog
mal_placee(P, U) :-
       final_state(Fin),
       % Récupération de l'élément de U aux coordonnées [L, C]
       coordonnees([L,C], U,   P),
       % Récupération de l'élément de F aux coordonnées [L, C]
       coordonnees([L,C], Fin, Q),
       % Tests : P est mal placé s'il ne correspond pas à Q, l'objectif
       P \= vide,
       P \= Q.
```

### Heuristique 2
Cette heuristique utilise la distance Manhattan pour compter le nombre de mouvements pour que chaque tuile atteigne son objectif. Là encore, j'ai défini une fonction pour calculer la distance Manhattan puis je somme toutes ces valeurs.

```prolog
poids_manhattan(P, U, Value) :-
       final_state(Fin),
       % Récupération des coordonnées de U et F pour un élément donné
       coordonnees([LU,CU], U,   P),
       coordonnees([LF,CF], Fin, P),
       P \= vide,
       % Calcul de la distance Manhattan
       Value is abs(LU - LF) + abs(CU - CF).
```

## Implémentation de A*

### Analyse expérimentale

À partir du cas trivial :
```prolog
initial_state(U) :-
    final_state(U).
```
l'algorithme A* implémenté trouve la solution :
```txt
### Solution found ###
Cost: "0"; Action: "nil"; State: "[[a,b,c],[h,vide,d],[g,f,e]]"
true .
```

Voici l'ensemble des résultats pour des grilles dont les séquences optimales sont entre 0 et 30 actions.

Heuristique | Taille de la séquence optimale | Temps
:----------:|:------------------------------:|:----:
H1          | 0                              | 0 ms
H1          | 1                              | 0 ms
H1          | 2                              | 1 ms
H1          | 5                              | 1 ms
H1          | 10                             | 5 ms
H1          | 20                             | 39 ms
H1          | 30                             | 1.638 s
H1          | *pas de solution*              | 1 ms
-           | -                              | -
H2          | 0                              | 0 ms
H2          | 1                              | 0 ms
H2          | 2                              | 0 ms
H2          | 5                              | 1 ms
H2          | 10                             | 2 ms
H2          | 20                             | 3 ms
H2          | 30                             | 5 ms
H2          | *pas de solution*              | 1 ms

Comme attendu, l'heuristique 2 qui représente la somme de toutes les distances de Manhattan entre la position actuelle et la position finale de chaque case permets à l'A* de converger beaucoup plus rapidement vers la solution.

Ça se voit plus facilement pour les séquences plus grandes, notamment pour une séquence de 30 coups : la seconde heuristique est 330 fois plus rapide que la première pour arriver à la solution.
