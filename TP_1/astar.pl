:- encoding(iso_latin_1).

%*******************************************************************************
%                                    ASTAR
%*******************************************************************************

/*
Rappels sur l'algorithme

- structures de donnees principales = 2 ensembles : P (etat pendants) et Q (etats clos)
- P est dedouble en 2 arbres binaires de recherche equilibres (AVL) : Pf et Pu

   Pf est l'ensemble des etats pendants (pending states), ordonnes selon
   f croissante (h croissante en cas d'egalite de f). Il permet de trouver
   rapidement le prochain etat a developper (celui qui a f(U) minimum).

   Pu est le meme ensemble mais ordonne lexicographiquement (selon la donnee de
   l'etat). Il permet de retrouver facilement n'importe quel etat pendant

   On gere les 2 ensembles de fa�on synchronisee : chaque fois qu'on modifie
   (ajout ou retrait d'un etat dans Pf) on fait la meme chose dans Pu.

   Q est l'ensemble des etats deja developpes. Comme Pu, il permet de retrouver
   facilement un etat par la donnee de sa situation.
   Q est modelise par un seul arbre binaire de recherche equilibre.

Predicat principal de l algorithme :

   astar(Pf,Pu,Q)

   - reussit si Pf est vide ou bien contient un etat minimum terminal
   - sinon on prend un etat minimum U, on genere chaque successeur S et les valeurs g(S) et h(S)
	 et pour chacun
		si S appartient a Q, on l oublie
		si S appartient a Ps (etat deja rencontre), on compare
			g(S)+h(S) avec la valeur deja calculee pour f(S)
			si g(S)+h(S) < f(S) on reclasse S dans Pf avec les nouvelles valeurs
				g et f
			sinon on ne touche pas a Pf
		si S est entierement nouveau on l insere dans Pf et dans Ps
	- appelle recursivement Star avec les nouvelles valeurs NewPF, NewPs, NewQs

*/

%*******************************************************************************

[avl].      % predicats pour gerer des arbres bin. de recherche
[taquin].   % predicats definissant le systeme a etudier

%*******************************************************************************

calculate_F(F, H, G) :-
    F is H + G.

main :-
    % Initialisation de la situation de d�part
    initial_state_4(S0),
    heuristique(S0, H0),
    G0 is 0,
    calculate_F(F0, H0, G0),

    % initialisations Pf, Pu et Q
    empty(Pf),
    empty(Pu),
    empty(Q),
    insert([[F0,H0,G0], S0], Pf, PfNew),
    insert([S0, [F0,H0,G0], nil, nil], Pu, PuNew),

    % lancement de AStar
    astar(PfNew, PuNew, Q).

%*******************************************************************************

build_path(Q, U, [(Cost, A, U) | Result]) :-
    suppress([U, Cost, Pere, A], Q, QNew),
    build_path(QNew, Pere, Result).
build_path(_, _, []).

display_solution(Q, U) :-
    build_path(Q, U, Tab),
    reverse(Tab, ToPrint),
    print_solution(ToPrint).

print_solution([]).
print_solution([(Cost, A, U) | Result]) :-
    print("Cost: "), print(Cost), print("; Action: "), print(A), print("; "), print(U),nl,
    print_solution(Result).

expand([[_Fu, _Hu, _Gu], State], ListSuccessors) :-
    findall([NextState, Rule, Cost], rule(Rule, Cost, State, NextState), ListSuccessors).

% Helper function: memorize the better evaluation
update_S_inside_P(Value1, Value2, _A, _S, Pf, Pu, Pf, Pu, _Pere) :-
    Value2 @< Value1, !.

update_S_inside_P([F1, H1, G1], [F2, H2, G2], A, S, Pf, Pu, PfNew, PuNew, Pere) :-
    suppress([[F2, H2, G2], S], Pf, PfInter),
    insert([[F1, H1, G1], S], PfInter, PfNew),
    suppress([S, [F2, H2, G2], _OldPere, _A], Pu, PuInter),
    insert([S, [F1, H1, G1], Pere, A], PuInter, PuNew).

loop_successors([], Pf, Pu, Pf, Pu, _, _Pere, _GPere).
loop_successors([[S, _Rule, _Cost] | Rest], Pf, Pu, Pf, Pu, Q, Pere, GPere) :-
    belongs(S, Q),
    loop_successors(Rest, Pf, Pu, Pf, Pu, Q, Pere, GPere).

loop_successors([[S, Rule, Cost] | Rest], Pf, Pu, PfNew, PuNew, Q, Pere, GPere) :-
    belongs([S, [Factual, H, G], _Pere, _Rule], Pu),
    heuristique(S, Hmaj),
    Gmaj is GPere + Cost,
    calculate_F(Fmaj, Hmaj, Gmaj),
    update_S_inside_P([Factual, H, G], [Fmaj, Hmaj, Gmaj], Rule, S, Pf, Pu, PfInter, PuInter, Pere),
    loop_successors(Rest, PfInter, PuInter, PfNew, PuNew, Q, Pere, GPere).

loop_successors([[S, Rule, Cost] | Rest], Pf, Pu, PfNew, PuNew, Q, Pere, GPere) :-
    heuristique(S, H),
    G is GPere + Cost,
    calculate_F(F, H, G),
    insert([[F, H, G], S], Pf, PfInter),
    insert([S, [F, H, G], Pere, Rule], Pu, PuInter),
    loop_successors(Rest, PfInter, PuInter, PfNew, PuNew, Q, Pere, GPere).

astar([], [], _) :-
    print("No solution: the final state is unreachable!").

astar(Pf, Pu, Q) :-
    suppress_min([V, F], Pf, _PfNew),
    V = [Fu, _Hu, _Gu],
    suppress([F, V, Pere, Action], Pu, _PuNew),
    final_state_4(F),!,
    insert([F, Fu, Pere, Action], Q, QFinal),
    display_solution(QFinal, F).

astar(Pf, Pu, Qs) :-
    suppress_min([V, U], Pf, PfInter),
    V = [_Fu, _Hu, Gu],
    suppress([U, V, Pere, Action], Pu, PuInter),
    expand([V, U], Successors),
    loop_successors(Successors, PfInter, PuInter, PfNew, PuNew, Qs, U, Gu),
    % Q est composé de noeuds dont le type est [U, G, Pere, A]
    insert([U, Gu, Pere, Action], Qs, QNew),
    astar(PfNew, PuNew, QNew).
