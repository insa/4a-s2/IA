:- encoding(iso_latin_1).

    /*
    Ce programme met en oeuvre l'algorithme Minmax (avec convention
    negamax) et l'illustre sur le jeu du TicTacToe (morpion 3x3)
    */

:- [tictactoe].


    /****************************************************
    ALGORITHME MINMAX avec convention NEGAMAX : negamax/5
    *****************************************************/

    /*
    negamax(+J, +Etat, +P, +Pmax, [?Coup, ?Val])

    SPECIFICATIONS :

    retourne pour un joueur J donne, devant jouer dans
    une situation donnee Etat, de profondeur donnee P,
    le meilleur couple [Coup, Valeur] apres une analyse
    pouvant aller jusqu a la profondeur Pmax.

    Il y a 3 cas a decrire (donc 3 clauses pour negamax/5)

    1/ la profondeur maximale est atteinte : on ne peut pas
    developper cet Etat ;
    il n y a donc pas de coup possible a jouer (Coup = rien)
    et l evaluation de Etat est faite par l'heuristique.

    2/ la profondeur maximale n est pas  atteinte mais J ne
    peut pas jouer ; au TicTacToe un joueur ne peut pas jouer
    quand le tableau est complet (totalement instancie) ;
    il n'y a pas de coup a jouer (Coup = rien)
    et l'evaluation de Etat est faite par l'heuristique.

    3/ la profondeur maxi n est pas atteinte et J peut encore
    jouer. Il faut evaluer le sous-arbre complet issu de Etat ;

    - on determine d abord la liste de tous les couples
    [Coup_possible, Situation_suivante] via le predicat
     successeurs/3 (deja fourni, voir plus bas).

    - cette liste est passee a un predicat intermediaire :
    loop_negamax/5, charge d appliquer negamax sur chaque
    Situation_suivante ; loop_negamax/5 retourne une liste de
    couples [Coup_possible, Valeur]

    - parmi cette liste, on garde le meilleur couple, c-a-d celui
    qui a la plus petite valeur (cf. predicat meilleur/2);
    soit [C1,V1] ce couple optimal. Le predicat meilleur/2
    effectue cette selection.

    - finalement le couple retourne par negamax est [Coup, V2]
    avec : V2 is -V1 (cf. convention negamax vue en cours).
.....................................
    */

negamax(J, Etat, Pmax, Pmax, [rien, Val]) :-
    heuristique(J, Etat, Val),
    !.
negamax(J, Etat, _P, _Pmax, [rien, Val]) :-
    situation_terminale(J, Etat),
    heuristique(J, Etat, Val),
    !.
negamax(J, Etat, P, Pmax, [Coup, Val]) :-
    successeurs(J, Etat, Succ),
    loop_negamax(J, P, Pmax, Succ, ListCouples),
    meilleur(ListCouples, [Coup,BestV]),
    Val is -BestV,
    !.

    /*******************************************
     DEVELOPPEMENT D'UNE SITUATION NON TERMINALE
     successeurs/3
     *******************************************/

     /*
     successeurs(+J,+Etat, ?Succ)

     retourne la liste des couples [Coup, Etat_Suivant]
     pour un joueur donne dans une situation donnee
     */

successeurs(J,Etat,Succ) :-
    copy_term(Etat, Etat_Suiv),
    findall([Coup,Etat_Suiv],
            successeur(J,Etat_Suiv,Coup),
            Succ).

    /*************************************
         Boucle permettant d'appliquer negamax
         a chaque situation suivante :
    *************************************/

    /*
    loop_negamax(+J,+P,+Pmax,+Successeurs,?Liste_Couples)
    retourne la liste des couples [Coup, Valeur_Situation_Suivante]
    a partir de la liste des couples [Coup, Situation_Suivante]
    */

loop_negamax(_,_, _  ,[],                []).
loop_negamax(J,P,Pmax,[[Coup,Suiv]|Succ],[[Coup,Vsuiv]|Reste_Couples]) :-
    loop_negamax(J,P,Pmax,Succ,Reste_Couples),
    adversaire(J,A),
    Pnew is P+1,
    negamax(A,Suiv,Pnew,Pmax, [_Coup,Vsuiv]).

    /*********************************
     Selection du couple qui a la plus
     petite valeur V
     *********************************/

    /*
    meilleur(+Liste_de_Couples, ?Meilleur_Couple)

    SPECIFICATIONS :
    On suppose que chaque element de la liste est du type [C,V]
    - le meilleur dans une liste qui a un seul element est cet element
    - le meilleur dans une liste [X|L] avec L \= [], est obtenu en comparant
      X et Y,le meilleur couple de L
      Entre X et Y on garde celui qui a la petite valeur de V.
    */

meilleur([Alone], Alone).
meilleur([[CX,VX] | L], Meilleur) :-
    L \= [],
    meilleur(L, [CY,VY]),
    ( VX =< VY ->
        Meilleur = [CX,VX]
    ;
        Meilleur = [CY,VY]
    ).

    /******************
    PROGRAMME PRINCIPAL
    *******************/

% Choix du meilleur coup � jouer par l'IA
ia_plays(IA, Situation, Pmax) :-
    negamax(IA, Situation, 0, Pmax, [Coup, H]),
    print("co�t heuristique : "), print(H), nl,
    successeur(IA, Situation, Coup).

% Situation initiale : le joueur peut d�cider de commencer ou non
ia_begins(non, Situation, o, Pmax) :-
    ia_plays(x, Situation, Pmax), !.
ia_begins(oui, _Situation, x, _Pmax) :- !.
ia_begins(Choice, _, _, _Pmax) :-
    write('Invalid choice: \''),write(Choice),write('\': exiting the program.'),nl,
    halt.

% Programme principal
main(Pmax) :-
    situation_initiale(Situation),
    write('Voulez-vous commencer ? (oui/non) : '),flush_output,
    read(Choice),
    ia_begins(Choice, Situation, Joueur, Pmax),
    adversaire(Joueur, IA),
    write('Vous jouez les \''),write(Joueur),write('\', l\'IA joue les \''),write(IA),write('\'.'),nl,
    main_loop(Joueur, IA, Joueur, Situation, Pmax),
    !.

% Match nul
main_loop(_Joueur, _IA, Current, Situation, _Pmax) :-
    situation_terminale(Current, Situation),
    nl,write('=== Match nul ==='),nl,
    display(Situation),
    !.
% Le joueur gagne
main_loop(Joueur, _IA, _Current, Situation, _Pmax) :-
    situation_gagnante(Situation, Joueur),
    nl,write('=== F�LICITATIONS ==='),nl,
    display(Situation),
    !.
% L'IA gagne
main_loop(Joueur, _IA, _Current, Situation, _Pmax) :-
    situation_perdante(Situation, Joueur),
    nl,write('=== VOUS AVEZ PERDU ==='),nl,
    display(Situation),
    !.
% Tour du joueur
main_loop(Joueur, IA, Joueur, Situation, Pmax) :-
    nl,writeln('## VOTRE TOUR ##    Situation actuelle :'),
    display(Situation),
    taille_situation(Situation, Size),
    write('Ligne   o� vous voulez jouer (max '),write(Size),write(') : '),flush_output,read(L),
    write('Colonne o� vous voulez jouer (max '),write(Size),write(') : '),flush_output,read(C),
    successeur(Joueur, Situation, [L,C]),
    main_loop(Joueur, IA, IA, Situation, Pmax).
% Tour de l'IA
main_loop(Joueur, IA, IA, Situation, Pmax) :-
    nl,writeln('## TOUR DE L\'IA ## Situation actuelle :'),
    display(Situation),
    ia_plays(IA, Situation, Pmax),
    main_loop(Joueur, IA, Joueur, Situation, Pmax).
