# TP2
## Familiarisation avec le problème du TicTacToe 3x3

**1.2 Quelle interprétation donnez-vous aux requêtes suivantes :**
```prolog
?- situation_initiale(S), joueur_initial(J).
S = [[_4586, _4592, _4598], [_4610, _4616, _4622], [_4634, _4640, _4646]],
J = x.
```
Cette requête permets d'initialiser la partie (état initial et premier joueur à jouer).

```prolog
?- situation_initiale(S), nth1(3,S,Lig), nth1(2,Lig,o).
S = [[_4070, _4076, _4082], [_4094, _4100, _4106], [_4118, o, _4130]],
Lig = [_4118, o, _4130].
```
Cette requête permets de placer un `o` sur la ligne 3, colonne 2.

**2.2 Compléter le programme pour définir les différentes formes d'alignement retournées par le prédicat `alignement(Ali ,Matrice)`**
Pour calculer les alignements dans les diagonales, j'ai préféré faire appel au prédicat `findall/3` pour trouver toutes les cases ayant la même colonne et la même ligne *(première diagonale)*, ou dont la somme vaut la taille de la grille *(deuxième diagonale)* :
```prolog
diagonale(D, M) :- findall(Elm, (nth1(L, M, Ligne), nth1(L, Ligne, Elm)), D).
diagonale(D, M) :- findall(Elm, (length(M, Size), nth1(L1, M, Ligne), L2 is Size - L1 + 1, nth1(L2, Ligne, Elm)), D).
```

L'appel à `alignement/3` donne bien le résultat attendu :
```prolog
?- M = [[1,2,3], [4,5,6], [7,8,9]], alignement(Ali, M).
Ali = [1, 2, 3] ;
Ali = [4, 5, 6] ;
Ali = [7, 8, 9] ;
Ali = [1, 4, 7] ;
Ali = [2, 5, 8] ;
Ali = [3, 6, 9] ;
Ali = [1, 5, 9] ;
Ali = [3, 5, 7].
```

**Définir le prédicat `possible(Ali, Joueur)`**
La définition suivante permets le comportement attendu du prédicat `possible/2` :
```prolog
possible([   ], _).
possible([X|L], J) :-
    unifiable(X,J),
    possible(L,J).
```

Enfin, après avoir défini les alignements gagnant et perdant, j'ai défini les situations gagnante et perdante :
```prolog
alignement_gagnant(Ali, J) :-
    possible(Ali, J),
    situation_terminale(J, Ali).

alignement_perdant(Ali, J1) :-
    adversaire(J1, J2),
    alignement_gagnant(Ali, J2).

situation_gagnante(Situation, J) :-
    alignement(Ali, Situation),
    alignement_gagnant(Ali, J).
situation_perdante(Situation, J) :-
    alignement(Ali, Situation),
    alignement_perdant(Ali, J).
```

## Développement de l'heuristique *h(Joueur, Situation)*
Dans le cas où une situation n'est ni gagnante ni perdante, l'heuristique se calcule comme étant la différence entre le nombre d'alignements possibles pour le joueur et le nombre d'alignements possibles pour l'adversaire :
```prolog
heuristique(J1,Situation,H) :-        % cas 3
    adversaire(J1,J2),
    findall(A, (alignement(A,Situation), possible(A,J1)), L1),
    findall(A, (alignement(A,Situation), possible(A,J2)), L2),
    length(L1, S1),
    length(L2, S2),
    H is S1 - S2,!.
```

Les heuristiques pour les deux joueurs correspondent bien lorsqu'une unique case est fixée :
```prolog
?- S = [[_,_,_], [_,x,_], [_,_,_]], heuristique(x,S,J1), heuristique(o,S,J2).
J1 = 4,
J2 = -4.
```
Dans les situations triviales (situation initiale ; situation finale ; situations gagnante, perdante ou nulle), l'heuristique retourne bien le bon résultat.

## Développement de l'algorithme Negamax
**3.2 - Quel prédicat permet de connaître sous forme de liste l'ensemble des couples `[Coord, Situation_Resultante]` ?**
Afin de pouvoir tester plus facilement, j'ai réalisé une interface d'entrée avec l'utilisateur dont l'algorithme est :
```txt
ia_plays(Situation, Profondeur) :
    negamax(Situation, Profondeur, L, C);
    update(Situation, L, C);

main(Profondeur) :
    S <- situation_initiale()
    Choix <- read('Voulez-vous commencer ? (oui/non)');
    si Choix = oui :
        Joueur <- x;
        IA <- o;
    sinon :
        Joueur <- o;
        IA <- x;
        ia_plays(S, Profondeur);
    fin si

    loop
        si situationTerminale(S) :
            print('Match nul');
            exit;
        sinon si situationGagnante(S) :
            print('Match gagné');
            exit;
        sinon si situationPerdante(S) :
            print('Match perdu');
            exit;
        sinon :
            si tourDe(IA) :
                ia_plays(S, Profondeur);
            sinon :
                L <- read('Ligne où jouer');
                C <- read('Colonne où jouer');
                update(S, L, C);
    fin loop;
```

Voici une exécution possible du programme final avec une profondeur de recherche du Negamax de 3 :
```prolog
?- main(3).
Voulez-vous commencer ? (oui/non) : non.
Vous jouez les 'o', l'IA joue les 'x'.

## VOTRE TOUR ##    Situation actuelle :
. . .
. x .
. . .
Ligne   où vous voulez jouer (max 3) : |    1.
Colonne où vous voulez jouer (max 3) : |    2.

## TOUR DE L'IA ## Situation actuelle :
. o .
. x .
. . .

## VOTRE TOUR ##    Situation actuelle :
. o .
x x .
. . .
Ligne   où vous voulez jouer (max 3) : |    2.
Colonne où vous voulez jouer (max 3) : |    3.

## TOUR DE L'IA ## Situation actuelle :
. o .
x x o
. . .

## VOTRE TOUR ##    Situation actuelle :
x o .
x x o
. . .
Ligne   où vous voulez jouer (max 3) : |    3.
Colonne où vous voulez jouer (max 3) : |    1.

## TOUR DE L'IA ## Situation actuelle :
x o .
x x o
o . .

=== MATCH PERDU ===
x o .
x x o
o . x
true.
```

## Expérimentation et extensions
**4.1 Quel est le meilleur coup à jouer et le gain espéré pour une profondeur d'analyse de 1, 2, 3, 4, 5, 6, 7, 8, 9**

Le meilleur coup à jouer est de prendre la case centrale au premier tour. Voici le gain espéré lors du premier tour en fonction de la profondeur d'analyse :

Profondeur | Gain
:---------:|:---:
1          | 4
2          | 1
3          | 3
4          | 1
5          | 3
6          | 1
7          | 2
8          | 10000
9          | 0

Lorsque Negamax est exécuté avec une profondeur de 8, l'algorithme considère qu'il va toujours gagner (gain infini) donc il y a une erreur dans le code actuel.

Il est facile de jouer sur une grille de n'importe quelle taille : il suffit de modifier la situation initiale par une matrice de la taille `N` souhaitée.
