:- encoding(iso_latin_1).

   /*********************************
    DESCRIPTION DU JEU DU TIC-TAC-TOE
    *********************************/

    /*
    Une situation est decrite par une matrice 3x3.
    Chaque case est soit un emplacement libre, soit contient le symbole d'un des 2 joueurs (o ou x)

    Contrairement a la convention du tp pr�c�dent, pour mod�liser une case libre
    dans une matrice on n'utilise pas une constante sp�ciale (ex : nil, 'vide', 'libre','inoccupee' ...);
    On utilise plut�t une variable libre (_), c'est�-dire un terme non instanci� ('_').
    La situation initiale est donc une matrice 3x3 composee uniquement de variables libres (_).
    Ceci est possible car le jeu consiste � instancier la grille avec des symboles et non � d�placer les symbles d�j� affect�s.

    Jouer un coup, c-a-d placer un symbole dans une grille S1 ne consiste pas � g�n�rer une nouvelle grille S2 obtenue
    en copiant d'abord S1 puis en remplacant le symbole de case libre par le symbole du joueur, mais plus simplement
    � INSTANCIER (au sens Prolog) la variable libre qui repr�sentait la case libre par la valeur associ�e au joueur, ex :
    Case = Joueur, ou a realiser indirectement cette instanciation par unification via un pr�dicat comme member/2, select/3, nth1/3 ...

    Ainsi si on joue un coup en S, S perd une variable libre, mais peut continuer � s'appeler S (on n'a pas besoin de la d�signer
    par un nouvel identificateur).
    La situation initiale est une "matrice" 3x3 (liste de 3 listes de 3 termes chacune)
    o� chacun des 9 termes est une variable libre.
    */

situation_initiale([ [_,_,_],
                     [_,_,_],
                     [_,_,_] ]).

taille_situation(Situation, Size) :-
    length(Situation, Size).

display_line([]) :-
    nl.
display_line([X|L]) :-
    var(X),
    write('. '),
    display_line(L),
    !.
display_line([X|L]) :-
    print(X), write(' '),
    display_line(L).

display([]).
display([Line|Rest]) :-
    display_line(Line),
    display(Rest).

    % Convention (arbitraire) : c'est x qui commence

joueur_initial(x).

    % Definition de la relation adversaire/2

adversaire(x,o).
adversaire(o,x).

    /****************************************************
     DEFINIR ICI � l'aide du pr�dicat ground/1 comment
     reconnaitre une situation terminale dans laquelle il
     n'y a aucun emplacement libre : aucun joueur ne peut
     continuer � jouer (quel qu il soit).
     ****************************************************/

situation_terminale(_Joueur, Situation) :- ground(Situation).

/********************************************************
 DEFINITIONS D UN ALIGNEMENT MAXIMAL DANS UNE MATRICE NxN
 ********************************************************/

alignement(L, Matrix) :- ligne(    L,Matrix).
alignement(C, Matrix) :- colonne(  C,Matrix).
alignement(D, Matrix) :- diagonale(D,Matrix).

ligne(L, M) :-
    nth1(_, M, L).

colonne_nth(C, M, N) :-
    findall(Elm, (nth1(_L, M, Ligne), nth1(N, Ligne, Elm)), C).

colonne(C, M) :-
    length(M, Size),
    between(1, Size, N),
    colonne_nth(C, M, N).

    /* D�finition de la relation liant une diagonale D � la matrice M dans laquelle elle se trouve.
        il y en a 2 sortes de diagonales dans une matrice carree(https://fr.wikipedia.org/wiki/Diagonale) :
        - la premiere diagonale (principale) (descendante) : (A I)
        - la seconde diagonale  (ascendante)  : (R Z)
        A . . . . . . . Z
        . \ . . . . . / .
        . . \ . . . / . .
        . . . \ . / . . .
        . . . . X . . .
        . . . / . \ . . .
        . . / . . . \ . .
        . / . . . . . \ .
        R . . . . . . . I*/

diagonale(D, M) :- findall(Elm, (nth1(L, M, Ligne), nth1(L, Ligne, Elm)), D).
    %premiere_diag(1,D,M).
diagonale(D, M) :- findall(Elm, (length(M, Size), nth1(L1, M, Ligne), L2 is Size - L1 + 1, nth1(L2, Ligne, Elm)), D).
    %seconde_diag(1,D,M).

premiere_diag(_,[],[]).
premiere_diag(K,[E|D],[Ligne|M]) :-
    nth1(K,Ligne,E),
    K1 is K+1,
    premiere_diag(K1,D,M).

% definition de la seconde diagonale A COMPLETER
% seconde_diag(K,M,D) :- true.

    /***********************************
     DEFINITION D'UN ALIGNEMENT POSSIBLE
     POUR UN JOUEUR DONNE
     ***********************************/

possible([   ], _).
possible([X|L], J) :- unifiable(X,J), possible(L,J).

    /* Attention
    il faut juste verifier le caractere unifiable
    de chaque emplacement de la liste, mais il ne
    faut pas realiser l unification.
    */

unifiable(X,_J) :-
    var(X),!.
unifiable(X,X).

    /**********************************
     DEFINITION D UN ALIGNEMENT GAGNANT
     OU PERDANT POUR UN JOUEUR DONNE J
     **********************************/

    /*
    Un alignement gagnant pour J est un alignement
possible pour J qui n'a aucun element encore libre.
Un alignement perdant pour J est gagnant
pour son adversaire.
    */

alignement_gagnant(Ali, J) :-
    possible(Ali, J),
    situation_terminale(J, Ali).

alignement_perdant(Ali, J1) :-
    adversaire(J1, J2),
    alignement_gagnant(Ali, J2).

situation_gagnante(Situation, J) :-
    alignement(Ali, Situation),
    alignement_gagnant(Ali, J).
situation_perdante(Situation, J) :-
    alignement(Ali, Situation),
    alignement_perdant(Ali, J).

    /******************************
    DEFINITION D'UN ETAT SUCCESSEUR
    *******************************/

    /*
    Il faut d�finir quelle op�ration subit
    une matrice M representant la situation courante
    lorsqu'un joueur J joue en coordonnees [L,C]
    */

successeur(J,Etat,[L,C]) :-
    nth1(L, Etat, Ligne),
    nth1(C, Ligne, X),
    var(X),
    X = J.

    /**************************************
     EVALUATION HEURISTIQUE D'UNE SITUATION
     **************************************/

    /*
1/ l'heuristique est +infini si la situation J est gagnante pour J
2/ l'heuristique est -infini si la situation J est perdante pour J
3/ sinon, on fait la difference entre :
       le nombre d'alignements possibles pour J
    moins
       le nombre d'alignements possibles pour l'adversaire de J
*/


heuristique(J,Situation,H) :-        % cas 1
   H = 10000,                % grand nombre approximant +infini
   alignement(Alig,Situation),
   alignement_gagnant(Alig,J), !.

heuristique(J,Situation,H) :-        % cas 2
   H = -10000,                % grand nombre approximant -infini
   alignement(Alig,Situation),
   alignement_perdant(Alig,J),!.

% on ne vient ici que si les cut precedents n'ont pas fonctionne,
% c-a-d si Situation n'est ni perdante ni gagnante.

heuristique(J1,Situation,H) :-        % cas 3
    adversaire(J1,J2),
    findall(A, (alignement(A,Situation), possible(A,J1)), L1),
    findall(A, (alignement(A,Situation), possible(A,J2)), L2),
    length(L1, S1),
    length(L2, S2),
    H is S1 - S2,!.

